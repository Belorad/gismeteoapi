﻿using GismeteoParser.Requests;

namespace GismeteoParser.Tests
{
	public class GetRequestTests
	{
		[Fact]
		public void GetRequestGismeteoTestNotNull()
		{
			//Arrange
			GismeteoRequestData requestData = new();

			//Act
			var html = GetRequest.Run(requestData, "https://www.gismeteo.ru/weather-sankt-peterburg-4079/10-days/").Response;

			//Assert
			Assert.NotNull(html);
		}

		[Fact]
		public void GetRequestGismeteoTestContains()
		{
			//Arrange
			TestingFragmentGetRequestGismeteo testHtml = new();
			GismeteoRequestData requestData = new();

			//Act
			var html = GetRequest.Run(requestData, "https://www.gismeteo.ru/weather-sankt-peterburg-4079/10-days/").Response;

			//Assert
			Assert.Contains(testHtml.HtmlFragment, html);
		}

		[Fact]
		public void GetRequestRamblerTestNotNull()
		{
			//Arrange
			RamblerRequestData requestData = new();

			//Act
			var html = GetRequest.Run(requestData, "https://weather.rambler.ru/api/v3/ndays/?n=10&url_path=v-sankt-peterburge").Response;

			//Assert
			Assert.NotNull(html);
		}

		[Fact]
		public void GetRequestRamblerTestContains()
		{
			//Arrange
			TestingFragmentGetRequestRambler testHtml = new();
			RamblerRequestData requestData = new();

			//Act
			var html = GetRequest.Run(requestData, "https://weather.rambler.ru/api/v3/ndays/?n=10&url_path=v-sankt-peterburge").Response;

			//Assert
			Assert.Contains(testHtml.HtmlFragment, html);
		}
	}
}
