using GismeteoParser.Models;
using GismeteoParser.Parsers;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace GismeteoParser.Tests
{
	public class ParsersTests
	{
		[Fact]
		public void SkyParserGismeteoTest()
		{
			//Arrange
			SkyParserGismeteo parser = new();
			TestingHtmlParsersGismeteo html = new();
			WeatherGismeteo data = new()
			{
					Sky =
					[
					"����",
					"����",
					"����",
					"�������, ���������  �����",
					"�������, ���������  �����",
					"�����������, ��� �������",
					"�������,  �����",
					"�����������,  �����",
					"����",
					"�������, ��� �������"
					]
			};

			//Act
			parser.ParseData(html.Html, out string[] sky);

			//Assert
			Assert.Equal(data.Sky, sky);
		}

		[Fact]
		public void MinTemperatureParserGismeteoTest()
		{
			//Arrange
			MinTemperatureParserGismeteo parser = new();
			TestingHtmlParsersGismeteo html = new();
			WeatherGismeteo data = new()
			{
					MinTemperature =
					[
					"12",
					"12",
					"14",
					"16",
					"16",
					"16",
					"15",
					"16",
					"14",
					"15"
					]
			};

			//Act
			parser.ParseData(html.Html, out string[] minTemperature);

			//Assert
			Assert.Equal(data.MinTemperature, minTemperature);
		}

		[Fact]
		public void MaxTemperatureParserGismeteoTest()
		{
			//Arrange
			MaxTemperatureParserGismeteo parser = new();
			TestingHtmlParsersGismeteo html = new();
			WeatherGismeteo data = new()
			{
					MaxTemperature =
					[
					"24",
					"25",
					"26",
					"21",
					"24",
					"24",
					"25",
					"24",
					"23",
					"21"
					]
			};

			//Act
			parser.ParseData(html.Html, out string[] maxTemperature);

			//Assert
			Assert.Equal(data.MaxTemperature, maxTemperature);
		}

		[Fact]
		public void MaxWindVelocityParserGismeteoTest()
		{
			//Arrange
			MaxWindVelocityParserGismeteo parser = new();
			TestingHtmlParsersGismeteo html = new();
			WeatherGismeteo data = new() {
				MaxWindVelocity = 
				[
				"6",
				"8",
				"10",
				"8",
				"7",
				"4",
				"5",
				"4",
				"7",
				"6"
				]
			};

			//Act
			parser.ParseData(html.Html, out string[] maxWindVelocity);

			//Assert
			Assert.Equal(data.MaxWindVelocity, maxWindVelocity);
		}

		[Fact]
		public void PrecipitationParserGismeteoTest()
		{
			//Arrange
			PrecipitationsParserGismeteo parser = new();
			TestingHtmlParsersGismeteo html = new();
			WeatherGismeteo data = new()
			{
				Precipitation =
				[
				"0",
				"0",
				"0",
				"0,3",
				"0,2",
				"0",
				"3,6",
				"14,6",
				"0",
				"0"
				]
			};

			//Act
			parser.ParseData(html.Html, out string[] precipitations);

			//Assert
			Assert.Equal(data.Precipitation, precipitations);
		}

		[Fact]
		public void WeatherParserRamblerDateTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			string[] data = new string[10];
			string[] dates =
			[
				"2024-09-08",
				"2024-09-09",
				"2024-09-10",
				"2024-09-11",
				"2024-09-12",
				"2024-09-13",
				"2024-09-14",
				"2024-09-15",
				"2024-09-16",
				"2024-09-17"
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] date);

			for (int i = 0; i < 10; i++)
			{
				data[i] = date[i].Date;
			}

			//Assert
			Assert.Equal(dates, data);
		}

		[Fact]
		public void WeatherParserRamblerTemperatureTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			int[] data = new int[10];
			int[] temperatures =
			[
				24,
				24,
				25,
				20,
				22,
				24,
				24,
				24,
				23,
				23
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] temperature);

			for (int i = 0; i < 10; i++)
			{
				data[i] = temperature[i].Temperature;
			}

			//Assert
			Assert.Equal(temperatures, data);
		}

		[Fact]
		public void WeatherParserRamblerWindSpeedTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			int[] data = new int[10];
			int[] windSpeeds =
			[
				2,
				3,
				5,
				3,
				3,
				3,
				3,
				3,
				4,
				4
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] windSpeed);

			for (int i = 0; i < 10; i++)
			{
				data[i] = windSpeed[i].WindSpeed;
			}

			//Assert
			Assert.Equal(windSpeeds, data);
		}

		[Fact]
		public void WeatherParserRamblerWindDirectionTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			string[] data = new string[10];
			string[] windDirections =
			[
				"S",
				"S",
				"SE",
				"SE",
				"S",
				"SE",
				"S",
				"S",
				"SE",
				"S"
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] windDirection);
			
			for (int i = 0; i < 10; i++)
			{
				data[i] = windDirection[i].WindDirection;
			}

			//Assert
			Assert.Equal(windDirections, data);
		}

		[Fact]
		public void WeatherParserRamblerHumidityTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			int[] data = new int[10];
			int[] humidities =
			[
				40,
				40,
				38,
				58,
				59,
				62,
				50,
				57,
				53,
				48
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] humidity);

			for (int i = 0; i < 10; i++)
			{
				data[i] = humidity[i].Humidity;
			}

			//Assert
			Assert.Equal(humidities, data);
		}

		[Fact]
		public void WeatherParserRamblerPressureTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			int[] data = new int[10];
			int[] pressures =
			[
				769,
				763,
				757,
				756,
				761,
				763,
				762,
				763,
				763,
				763
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] pressure);

			for (int i = 0; i < 10; i++)
			{
				data[i] = pressure[i].Pressure;
			}

			//Assert
			Assert.Equal(pressures, data);
		}

		[Fact]
		public void WeatherParserRamblerIconTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			string[] data = new string[10];
			string[] icons =
			[
				"partly-cloudy",
				"partly-cloudy",
				"partly-cloudy",
				"cloudy",
				"cloudy",
				"cloudy",
				"partly-cloudy",
				"light-rain",
				"cloudy",
				"cloudy"
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] icon);

			for (int i = 0; i < 10; i++)
			{
				data[i] = icon[i].Icon;
			}

			//Assert
			Assert.Equal(data, data);
		}

		[Fact]
		public void WeatherParserRamblerPrecipitationTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			double[] data = new double[10];
			double[] precipitations =
			[
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0.4,
				0,
				0
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] precipitation);

			for (int i = 0; i < 10; i++)
			{
				data[i] = precipitation[i].Precipitation;
			}

			//Assert
			Assert.Equal(precipitations, data);
		}

		[Fact]
		public void WeatherParserRamblerCityNameTest()
		{
			//Arrange
			WeatherParserRambler parser = new();
			TestingJsonParserRambler json = new();
			string[] data = new string[10];
			string[] cities =
			[
				"�����-���������",
				"�����-���������",
				"�����-���������",
				"�����-���������",
				"�����-���������",
				"�����-���������",
				"�����-���������",
				"�����-���������",
				"�����-���������",
				"�����-���������",
			];

			//Act
			parser.ParseData(json.Json, out WeatherRamblerMongo[] city);

			for (int i = 0; i < 10; i++)
			{
				data[i] = city[i].City;
			}

			//Assert
			Assert.Equal(cities, cities);
		}
	}
}