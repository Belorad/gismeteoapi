﻿using MongoDB.Bson;

namespace GismeteoParser.Models
{
	internal class CitiesRamblerMongo
	{
		public ObjectId _id { get; set; }
		public string Href { get; set; } = string.Empty;
		public string Name { get; set; } = string.Empty;
	}
}