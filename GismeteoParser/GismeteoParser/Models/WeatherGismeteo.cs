﻿namespace GismeteoParser.Models
{
	public class WeatherGismeteo
	{
		public DateTime[] Date { get; set; } = new DateTime[10];
		public string[] Sky { get; set; } = new string[10];
		public string[] MinTemperature { get; set; } = new string[10];
		public string[] MaxTemperature { get; set; } = new string[10];
		public string[] MaxWindVelocity { get; set; } = new string[10];
		public string[] Precipitation { get; set; } = new string[10];
	}
}
