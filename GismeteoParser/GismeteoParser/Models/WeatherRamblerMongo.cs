﻿namespace GismeteoParser.Models
{
	public class WeatherRamblerMongo
	{
		public string City { get; set; } = string.Empty;
		public string Date { get; set; } = string.Empty;
		public int Temperature { get; set; } = new int();
		public int WindSpeed { get; set; } = new int();
		public string WindDirection { get; set; } = string.Empty;
		public int Humidity { get; set; } = new int();
		public int Pressure { get; set; } = new int(); 
		public string Icon { get; set; } = string.Empty;
		public double Precipitation { get; set; } = new double();
	}
}
