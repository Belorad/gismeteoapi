﻿namespace GismeteoParser.Models
{
	public class WeatherRambler
	{
		public string City { get; set; } = String.Empty;
		public string[] Date { get; set; } = new string[10];
		public int[] Temperature { get; set; } = new int[10];
		public int[] WindSpeed { get; set; } = new int[10];
		public string[] WindDirection { get; set; } = new string[10];
		public int[] Humidity { get; set; } = new int[10];
		public int[] Pressure { get; set; } = new int[10];
		public string[] Icon { get; set; } = new string[10];
		public double[] Precipitation { get; set; } = new double[10];
	}
}
