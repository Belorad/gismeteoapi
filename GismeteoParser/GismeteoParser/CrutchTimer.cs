﻿using GismeteoParser.Parsers;

namespace GismeteoParser
{
	internal class CrutchTimer
	{
		internal static void Run(ParserGismeteo parserGismeteo, ParserRambler parserRambler, int span)
		{
			int counter = 0;

			while (true)
			{
				Thread.Sleep(1000);
				counter++;

				if (counter == span)
				{
					Console.ForegroundColor = ConsoleColor.Yellow;
					Console.WriteLine("Начато взятие данных погоды.");
					Console.ResetColor();

					parserGismeteo.Parse();
					parserRambler.Parse();

					counter = 0;

					Console.ForegroundColor = ConsoleColor.Yellow;
					Console.WriteLine($"Таймер перезапущен в {DateTime.Now}");
					Console.ResetColor();
				}
			}
		}
	}
}
