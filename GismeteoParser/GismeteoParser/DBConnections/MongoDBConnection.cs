﻿using MongoDB.Driver;

namespace GismeteoParser.DBConnections
{
	internal class MongoDBConnection
	{
		private static readonly ConnectionConfigurationGeneral connection = new();
		static readonly string connectionString = connection.GetConnectionString("Compose-MongoDB");
		public static MongoClient client = new(connectionString);
	}
}
