﻿using GismeteoParser.Entities;
using Microsoft.Extensions.Configuration;

namespace GismeteoParser.DBConnections
{
	internal class ConnectionConfigurationEF : ConnectionConfiguration<DbContextOptions<ApplicationContext>>
	{
		public override DbContextOptions<ApplicationContext> GetConnectionString(string DbName)
		{
			var builder = new ConfigurationBuilder();
			// установка пути к текущему каталогу
			builder.SetBasePath(Directory.GetCurrentDirectory());
			// получаем конфигурацию из файла appsettings
			builder.AddJsonFile("appsettings.Development.json");
			// создаем конфигурацию
			var config = builder.Build();
			// получаем строку подключения
			var connectionString = config.GetConnectionString($"{DbName}");

			var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>();
			var options = optionsBuilder
				//.UseSqlServer(connectionString)
				.UseNpgsql(connectionString)
				.Options;

			return options;
		}
	}
}
