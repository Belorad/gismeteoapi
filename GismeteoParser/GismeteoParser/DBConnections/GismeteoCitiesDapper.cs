﻿using Dapper;
using GismeteoParser.Entities;
using Npgsql;
using System.Data;

namespace GismeteoParser.DBConnections
{
	internal class GismeteoCitiesDapper : IDataGetting<List<Cities>>
	{
		public void GetDataFromDB(out List<Cities> cities)
		{
			ConnectionConfigurationGeneral connection = new();
			//using IDbConnection db = new SqlConnection(connection.GetConnectionString("MSSQL"));
			using IDbConnection db = new NpgsqlConnection(connection.GetConnectionString("Postgres"));

			cities = [];

			try
			{
				//Cities[] cities = db.Query<Cities>("SELECT * FROM Cities").ToList();
				cities = db.Query<Cities>("SELECT * FROM \"Cities\"").ToList();

				Console.ForegroundColor = ConsoleColor.Cyan;
				Console.WriteLine("Данные городов успешно взяты из БД Postgres для парсинга Gismeteo.\n");
				Console.ResetColor();
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"{ex.Message}\n");
				Console.ResetColor();
			}
		}
	}
}
