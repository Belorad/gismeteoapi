﻿namespace GismeteoParser.DBConnections
{
	internal interface IDataUploading<T, K>
	{
		Task<bool> LoadDataToDB(T[] data, K id);
	}
}
