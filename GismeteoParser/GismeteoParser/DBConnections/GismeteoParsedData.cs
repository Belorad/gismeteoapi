﻿using GismeteoParser.Entities;

namespace GismeteoParser.DBConnections
{
	internal class GismeteoParsedData : IDataUploading<Weather, int>
	{
		public async Task<bool> LoadDataToDB(Weather[] data, int id)
		{
			ConnectionConfigurationEF connection = new();

			try
			{
				//await using ApplicationContext context = new(connection.GetConnectionString("MSSQL"));
				await using ApplicationContext context = new(connection.GetConnectionString("Compose-Postgres"));

				List<Weather>? weatherData = await context.Weather!
					.Where(c => c.CitiesId == id)
					.ToListAsync();

				for (int i = 0; i < 10; i++)
				{
					DateTime day = DateTime.Today.AddDays(i);
					Weather? deletingData = weatherData.Find(w => w.Date == day);

					if (deletingData != null)
					{
						context.Weather!.Remove(deletingData);
					}
				}

				foreach (var weather in data)
				{
					context.Weather!.Add(weather);
				}

				await context.SaveChangesAsync();

				return true;
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"{ex.Message}\n");
				Console.ResetColor();

				return false;
			}
		}
	}
}
