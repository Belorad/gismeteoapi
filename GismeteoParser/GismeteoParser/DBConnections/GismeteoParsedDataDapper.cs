﻿using Dapper;
using GismeteoParser.Entities;
using Npgsql;
using System.Data;

namespace GismeteoParser.DBConnections
{
	internal class GismeteoParsedDataDapper : IDataUploading<Weather, int>
	{
		public async Task<bool> LoadDataToDB(Weather[] data, int id)
		{
			ConnectionConfigurationGeneral connection = new();
			try
			{
				//using IDbConnection db = new SqlConnection(connection.GetConnectionString("MSSQL"));
				using IDbConnection db = new NpgsqlConnection(connection.GetConnectionString("Postgres"));

				//List<Weather>? weatherData = db.Query<Weather>($"SELECT * FROM Weather WHERE CitiesId = {id}").ToList();
				List<Weather>? weatherData = db.Query<Weather>($"SELECT * FROM \"Weather\" WHERE \"CitiesId\" = {id}").ToList();

				for (int i = 0; i < 10; i++)
				{
					DateTime day = DateTime.Today.AddDays(i);
					Weather? deletingData = weatherData.Find(w => w.Date == day);

					if (deletingData != null)
					{
						//var query = $"DELETE FROM Weather WHERE CitiesId = {id} AND Date = '{day}'";
						var query = $"DELETE FROM \"Weather\" WHERE \"CitiesId\" = {id} AND \"Date\" = '{day}'";

						await db.ExecuteAsync(query);
					}
				}

				foreach (var weather in data)
				{
					//var query = $"INSERT INTO Weather VALUES ('{weather.Date}', '{weather.Sky}', '{weather.MinTemperature}', '{weather.MaxTemperature}', '{weather.MaxWindVelocity}', '{weather.Precipitation}', {weather.CitiesId})";
					var query = $"INSERT INTO \"Weather\" VALUES ('{weather.Date}', '{weather.Sky}', '{weather.MinTemperature}', '{weather.MaxTemperature}', '{weather.MaxWindVelocity}', '{weather.Precipitation}', {weather.CitiesId})";

					await db.ExecuteAsync(query);
				}
				
				return true;
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"{ex.Message}\n");
				Console.ResetColor();

				return false;
			}
		}
	}
}
