﻿namespace GismeteoParser.DBConnections
{
	internal abstract class ConnectionConfiguration<T>
	{
		public abstract T GetConnectionString(string DbName);
	}
}
