﻿using GismeteoParser.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace GismeteoParser.DBConnections
{
	internal class RamblerParsedDataMongoDB : IDataUploading<WeatherRamblerMongo, ObjectId>
	{
		public async Task<bool> LoadDataToDB(WeatherRamblerMongo[] weather, ObjectId id)
		{
			try
			{
				var db = MongoDBConnection.client.GetDatabase("RamblerWeather");
				var collection = db.GetCollection<WeatherRamblerMongo>("Weather");

				foreach (var day in weather)
				{
					var filter = new BsonDocument { { "City", $"{day.City}" }, { "Date", $"{day.Date}" } };
					var options = new ReplaceOptions { IsUpsert = true };

					await collection.ReplaceOneAsync(filter, day, options);
				}

				return true;
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"{ex.Message}\n");
				Console.ResetColor();

				return false;
			}
		}
	}
}
