﻿using GismeteoParser.Models;
using MongoDB.Driver;

namespace GismeteoParser.DBConnections
{
	internal class RamblerCitiesMongoDB : IDataGetting<List<CitiesRamblerMongo>>
	{
		public void GetDataFromDB(out List<CitiesRamblerMongo> cities)
		{
			cities = [];
			
			try
			{
				var db = MongoDBConnection.client.GetDatabase("RamblerWeather");
				var collection = db.GetCollection<CitiesRamblerMongo>("Cities");

				cities = collection.Find("{}")
					.ToList();

				Console.ForegroundColor = ConsoleColor.Cyan;
				Console.WriteLine("Данные городов успешно взяты из коллекции Cities БД MongoDB для парсинга Rambler.\n");
				Console.ResetColor();
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"{ex.Message}\n");
				Console.ResetColor();
			}
		}
	}
}
