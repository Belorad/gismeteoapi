﻿using GismeteoParser.Entities;

namespace GismeteoParser.DBConnections
{
	internal class GismeteoCities : IDataGetting<List<Cities>>
	{
		public void GetDataFromDB(out List<Cities> cities)
		{
			ConnectionConfigurationEF connection = new();

			//using ApplicationContext context = new(connection.GetConnectionString("MSSQL"));
			using ApplicationContext context = new(connection.GetConnectionString("Compose-Postgres"));
			
			cities = [];

			try
			{
				cities = [.. context.Cities!.AsNoTracking()];

				Console.ForegroundColor = ConsoleColor.Cyan;
				Console.WriteLine("Данные городов успешно взяты из БД Postgres для парсинга Gismeteo.\n");
				Console.ResetColor();
			}
			catch(Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"{ex.Message}\n");
				Console.ResetColor();
			}
		}
	}
}
