﻿using GismeteoParser.Entities;

namespace GismeteoParser.DBConnections
{
	internal interface IDataGetting<T>
	{
		void GetDataFromDB(out T data);
	}
}
