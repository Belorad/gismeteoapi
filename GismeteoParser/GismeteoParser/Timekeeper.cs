﻿using GismeteoParser.Parsers;
using System.Timers;
using Timer = System.Timers.Timer;
using ThreadingTimer = System.Threading.Timer;

namespace GismeteoParser
{
	internal class Timekeeper
	{
		public static Timer aTimer;

		public static void RunParserWithTimer(int span)
		{
			aTimer = new Timer(span);
			aTimer.Elapsed += OnTimedEvent;

			aTimer.AutoReset = true;
			aTimer.Enabled = true;
		}

		public static void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			ParserGismeteo parserGismeteo = new();
			ParserRambler parserRambler = new();

			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine("Начато взятие данных погоды.");
			Console.ResetColor();

			parserGismeteo.Parse();
			parserRambler.Parse();
		}

		public static void RunParserWithThreadingTimer(int span)
		{
			TimerCallback tm = new(ExecuteEvent);

			ThreadingTimer stateTimer = new(tm, null, 10000, span);

			Console.ReadLine();
		}

		public static void ExecuteEvent(object obj)
		{
			ParserGismeteo parserGismeteo = new();
			ParserRambler parserRambler = new();

			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine("Начато взятие данных погоды.");
			Console.ResetColor();

			parserGismeteo.Parse();
			parserRambler.Parse();
		}
	}
}
