﻿using System.Net;

namespace GismeteoParser.Requests
{
	public class RamblerRequestData : IRequestData
	{
		public string? Response { get; set; }
		public string Accept { get; set; } = "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7";
		public string Host { get; set; } = "weather.rambler.ru";
		public string UserAgent { get; set; } = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36";
		//public WebProxy? Proxy { get; set; } = new WebProxy("127.0.0.1:8888");
		public WebProxy? Proxy { get; set; } = null;

		public Dictionary<string, string>? Headers { get; set; } = new Dictionary<string, string>
		{
			{"sec-ch-ua", "\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\", \"Google Chrome\";v=\"114\"" },
			{"sec-ch-ua-mobile", "?0"},
			{"sec-ch-ua-platform", "\"Windows\""},
			{"Sec-Fetch-Dest", "document"},
			{"Sec-Fetch-Mode", "navigate"},
			{"Sec-Fetch-Site", "same-origin"},
			{"Sec-Fetch-User", "?1"},
			{"Upgrade-Insecure-Requests", "1"}
		};
	}
}
