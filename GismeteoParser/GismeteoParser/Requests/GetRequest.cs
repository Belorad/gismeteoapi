﻿namespace GismeteoParser.Requests
{
	public static class GetRequest
	{
		static public IRequestData Run(IRequestData requestData, string address)
		{
			using var request = new HttpRequestMessage(HttpMethod.Get, address);

			request.Headers.Add("Accept", requestData.Accept);
			request.Headers.Host = requestData.Host;
			request.Headers.Add("User-Agent", requestData.UserAgent);

			foreach (var pair in requestData.Headers)
			{
				request.Headers.Add(pair.Key, pair.Value);
			}

			using var response = Client.client.Send(request);

			var content = response.Content.ReadAsStream();
			requestData.Response = new StreamReader(content).ReadToEnd();

			return requestData;
		}
	}
}
