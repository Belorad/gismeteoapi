﻿using System.Net;

namespace GismeteoParser.Requests
{
	public interface IRequestData
	{
		public string? Response { get; set; }
		public string Accept { get; set; }
		public string Host { get; set; }
		public string UserAgent { get; set; }
		public WebProxy? Proxy { get; set; }
		public Dictionary<string, string>? Headers { get; set; }
	}
}
