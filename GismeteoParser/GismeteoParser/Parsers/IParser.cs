﻿namespace GismeteoParser.Parsers
{
	internal interface IParser
	{
		void Parse();
	}
}
