﻿namespace GismeteoParser.Parsers
{
	public class MaxTemperatureParserGismeteo : IDataParser<string, string>
	{
		public void ParseData(string html, out string[] maxTemperature)
		{
			maxTemperature = new string[10];
			int tempIndex = html.IndexOf("temperature-value reactive show-units");

			for (int i = 0; i < 10; i++)
			{
				int tempStartA = html.IndexOf("maxt", tempIndex);
				int tempStartB = html.IndexOf("temperature-value value=\"", tempStartA) + 25;
				int tempEnd = html.IndexOf("\" from-unit=", tempStartB);
				maxTemperature[i] = html.Substring(tempStartB, tempEnd - tempStartB).Trim();
				tempIndex = tempEnd;
			}
		}
	}
}
