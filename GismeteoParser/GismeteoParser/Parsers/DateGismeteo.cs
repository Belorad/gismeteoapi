﻿using GismeteoParser.Models;

namespace GismeteoParser.Parsers
{
	internal class DateGismeteo
	{
		public static void GetDate(out DateTime[] date)
		{
			date = new DateTime[10];
			
			for (int i = 0; i < 10; i++)
			{
				date[i] = DateTime.SpecifyKind(DateTime.Today.AddDays(i), DateTimeKind.Utc);
			}
		}
	}
}
