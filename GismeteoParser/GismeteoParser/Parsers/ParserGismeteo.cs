﻿using GismeteoParser.DBConnections;
using GismeteoParser.Entities;
using GismeteoParser.Models;
using GismeteoParser.Requests;

namespace GismeteoParser.Parsers
{
	internal class ParserGismeteo : IParser
	{
		public void Parse()
		{
			GismeteoCities gismeteoCities = new();
			//GismeteoCitiesDapper gismeteoCities = new();

			gismeteoCities.GetDataFromDB(out List<Cities> cities);

			if (cities.Count > 0)
			{
				foreach (var city in cities)
				{
					GismeteoRequestData requestData = new();

					var html = GetRequest.Run(requestData, $"https://www.gismeteo.ru/{city.CityPath}/10-days/").Response;

					Console.ForegroundColor = ConsoleColor.Green;
					Console.WriteLine($"Данные погоды за 10 дней города {city.Name} успешно взяты.");

					if (html != null)
					{
						SkyParserGismeteo sky = new();
						MinTemperatureParserGismeteo minTemperature = new();
						MaxTemperatureParserGismeteo maxTemperature = new();
						MaxWindVelocityParserGismeteo maxWindVelocity = new();
						PrecipitationsParserGismeteo precipitations = new();

						DateGismeteo.GetDate(out DateTime[] date);

						sky.ParseData(html, out string[] skyData);
						minTemperature.ParseData(html, out string[] minTemperatureData);
						maxTemperature.ParseData(html, out string[] maxTemperatureData);
						maxWindVelocity.ParseData(html, out string[] maxWindVelocityData);
						precipitations.ParseData(html, out string[] precipitationsData);

						Weather[] newWeatherData = new Weather[10];

						for (int i = 0; i < 10; i++)
						{
							newWeatherData[i] = new()
							{
								Date = date[i],
								Sky = skyData[i],
								MinTemperature = minTemperatureData[i],
								MaxTemperature = maxTemperatureData[i],
								MaxWindVelocity = maxWindVelocityData[i],
								Precipitation = precipitationsData[i],
								CitiesId = city.Id
							};
						}

						GismeteoParsedData parsedData = new();
						//GismeteoParsedDataDapper parsedData = new();

						if (parsedData.LoadDataToDB(newWeatherData, city.Id).Result)
						{
							Console.ForegroundColor = ConsoleColor.Green;
							Console.WriteLine($"Данные погоды за 10 дней города {city.Name} успешно загружены.");
						}
						else
						{
							Console.ForegroundColor = ConsoleColor.Yellow;
							Console.WriteLine($"Данные погоды за 10 дней города {city.Name} не загружены.");
							Console.ResetColor();
						}
					}
					else
					{
						Console.ForegroundColor = ConsoleColor.Green;
						Console.WriteLine($"Данные погоды за 10 дней города {city.Name} не загружены.");
					}
				}
			}
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine($"Парсер данных погоды Gismeteo отработал. Дата и время: {DateTime.Now}\n");
			Console.ResetColor();
		}
	}
}
