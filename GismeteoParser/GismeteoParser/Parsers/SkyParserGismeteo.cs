﻿namespace GismeteoParser.Parsers
{
	public class SkyParserGismeteo : IDataParser<string, string>
	{
		public void ParseData(string html, out string[] sky)
		{
			sky = new string[10];
			int skyIndex = html.IndexOf("data-row=\"icon-tooltip\"");

			for (int i = 0; i < 10; i++)
			{
				int weatherStart = html.IndexOf("data-tooltip=\"", skyIndex) + 14;
				int weatherEnd = html.IndexOf("\">", weatherStart);
				sky[i] = html.Substring(weatherStart, weatherEnd - weatherStart)
					.Trim();
				skyIndex = weatherEnd;
			}
		}
	}
}
