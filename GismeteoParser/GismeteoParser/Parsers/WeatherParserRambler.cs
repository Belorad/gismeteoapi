﻿using GismeteoParser.Models;
using Newtonsoft.Json.Linq;

namespace GismeteoParser.Parsers
{
	public class WeatherParserRambler : IDataParser<string, WeatherRamblerMongo>
	{
		public void ParseData(string html, out WeatherRamblerMongo[] weather)
		{
			int i = 0;

			weather = new WeatherRamblerMongo[10];

			var json = JObject.Parse(html);

			var breadcrumbs = json["breadcrumbs"][3];

			var rangeWeather = json["range_weather"];

			foreach (var day in rangeWeather)
			{
				weather[i] = new()
				{
					City = breadcrumbs["name"]
						.ToString(),

					Date = day["date"]
						.ToString(),

					Temperature = day["forecast"]["day"]
						.Value<int>("temperature"),

					WindSpeed = day["forecast"]["day"]
						.Value<int>("wind_speed"),

					WindDirection = day["forecast"]["day"]["wind_direction"]
						.ToString(),

					Humidity = day["forecast"]["day"]
						.Value<int>("humidity"),

					Pressure = day["forecast"]["day"]
						.Value<int>("pressure"),

					Icon = day["forecast"]["day"]["icon"]
						.ToString(),

					Precipitation = day["forecast"]["day"]
						.Value<double>("precipitation")
				};
				
				i++;
			}
		}
	}
}
