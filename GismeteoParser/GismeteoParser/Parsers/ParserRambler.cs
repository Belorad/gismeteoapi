﻿using GismeteoParser.DBConnections;
using GismeteoParser.Models;
using GismeteoParser.Requests;

namespace GismeteoParser.Parsers
{
	internal class ParserRambler : IParser
	{
		public void Parse()
		{
			RamblerCitiesMongoDB ramblerCities = new();

			ramblerCities.GetDataFromDB(out List<CitiesRamblerMongo> cities);

			if (cities.Count > 0)
			{
				foreach (var city in cities)
				{
					RamblerRequestData requestData = new();

					var html = GetRequest.Run(requestData, $"https://weather.rambler.ru/api/v3/ndays/?n=10&url_path={city.Href}").Response;

					if (html != null)
					{
						Console.ForegroundColor = ConsoleColor.Green;
						Console.WriteLine($"Данные погоды за 10 дней города {city.Name} успешно взяты.");

						WeatherParserRambler parserRambler = new();
						parserRambler.ParseData(html, out WeatherRamblerMongo[] weather);

						RamblerParsedDataMongoDB parsedData = new();
						
						if (parsedData.LoadDataToDB(weather, city._id).Result)
						{
							Console.ForegroundColor = ConsoleColor.Green;
							Console.WriteLine($"Данные погоды за 10 дней города {city.Name} успешно загружены.");
						}
						else
						{
							Console.ForegroundColor = ConsoleColor.Yellow;
							Console.WriteLine($"Данные погоды за 10 дней города {city.Name} не загружены.");
							Console.ResetColor();
						}
					}
					else
					{
						Console.ForegroundColor = ConsoleColor.Green;
						Console.WriteLine($"Данные погоды за 10 дней города {city.Name} не загружены.");
					}
				}
			}

			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine($"Парсер данных погоды Rambler отработал. Дата и время: {DateTime.Now}\n");
			Console.ResetColor();
		}
	}
}
