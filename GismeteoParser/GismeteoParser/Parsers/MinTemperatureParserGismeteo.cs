﻿namespace GismeteoParser.Parsers
{
	public class MinTemperatureParserGismeteo : IDataParser<string, string>
	{
		public void ParseData(string html, out string[] minTemperature)
		{
			minTemperature = new string[10];
			int tempIndex = html.IndexOf("temperature-value reactive show-units");

			for (int i = 0; i < 10; i++)
			{
				int tempStartA = html.IndexOf("mint", tempIndex);
				int tempStartB = html.IndexOf("temperature-value value=\"", tempStartA) + 25;
				int tempEnd = html.IndexOf("\" from-unit=", tempStartB);
				minTemperature[i] = html.Substring(tempStartB, tempEnd - tempStartB).Trim();
				tempIndex = tempEnd;
			}
		}
	}
}
