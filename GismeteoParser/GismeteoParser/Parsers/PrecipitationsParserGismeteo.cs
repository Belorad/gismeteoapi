﻿namespace GismeteoParser.Parsers
{
	public class PrecipitationsParserGismeteo : IDataParser<string, string>
	{
		public void ParseData(string html, out string[] precipitations)
		{
			precipitations = new string[10];
			int rainIndex = html.IndexOf("Осадки в жидком эквиваленте, мм");

			for (int i = 0; i < 10; i++)
			{
				int rainStartA = html.IndexOf("item-unit", rainIndex);
				int rainStartB = html.IndexOf("\">", rainStartA) + 2;
				int rainEnd = html.IndexOf("</div>", rainStartB);
				precipitations[i] = html.Substring(rainStartB, rainEnd - rainStartB)
					.Trim();
				rainIndex = rainEnd;
			}
		}
	}
}
