﻿using GismeteoParser.Models;

namespace GismeteoParser.Parsers
{
	public class MaxWindVelocityParserGismeteo : IDataParser<string, string>
	{
		public void ParseData(string html, out string[] maxWindVelocity)
		{
			maxWindVelocity = new string[10];
			int windIndex = html.IndexOf("Порывы ветра,");

			for (int i = 0; i < 10; i++)
			{
				//int noData = html.IndexOf("item-nodata", windIndex, 332);
				//if (noData != -1)
				//{
				//	maxWindVelocity.MaxWindVelocity[i] = "-";
				//	windIndex += 181;
				//}
				//else
				//{
				//	int windStart = html.IndexOf("wind-unit unit unit_wind_m_s", windIndex) + 30;
				//	int windEnd = html.IndexOf("<", windStart);

				//	string temp = html.Substring(windStart, windEnd - windStart)
				//		.Trim();
				//	if (temp.IndexOf("ing", 0) == -1)
				//	{
				//		maxWindVelocity.MaxWindVelocity[i] = temp;
				//	}
				//	else
				//	{
				//		int indexStart = temp.IndexOf(">", 0) + 1;
				//		int indexEnd = temp.Count();
				//		maxWindVelocity.MaxWindVelocity[i] = temp.Substring(indexStart, indexEnd - indexStart).Trim();
				//	}

				//	windIndex = windEnd;
				//}

				int windStart = html.IndexOf("speed-value value=\"", windIndex) + 19;
				int windEnd = html.IndexOf("\" from-unit=", windStart);
				maxWindVelocity[i] = html.Substring(windStart, windEnd - windStart)
					.Trim();
				windIndex = windEnd;
			}
		}
	}
}
