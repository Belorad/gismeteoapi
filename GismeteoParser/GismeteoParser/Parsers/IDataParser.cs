﻿namespace GismeteoParser.Parsers
{
	internal interface IDataParser<H, T>
	{
		void ParseData(H html, out T[] data);
	}
}
