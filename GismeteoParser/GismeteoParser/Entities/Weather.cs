﻿namespace GismeteoParser.Entities
{
	internal class Weather
	{
		public int Id { get; set; }
		public DateTime Date { get; set; }
		public required string Sky { get; set; }
		public required string MinTemperature { get; set; }
		public required string MaxTemperature { get; set; }
		public required string MaxWindVelocity { get; set; }
		public required string Precipitation { get; set; }

		//внешний ключ
		public int CitiesId { get; set; }
		//навигационное свойство
		public Cities? City { get; set; }
	}
}
