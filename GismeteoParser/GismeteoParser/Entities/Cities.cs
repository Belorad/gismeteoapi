﻿namespace GismeteoParser.Entities
{
	internal class Cities
	{
		public int Id { get; set; }

		[Column(TypeName = "varchar(70)")]
		[Required]
		public required string Name { get; set; }

		[Column(TypeName = "varchar(70)")]
		[Required]
		public required string CityPath { get; set; }

		//навигационное свойство
		public List<Weather>? Weather { get; set; }
	}
}