﻿using GismeteoParser;
using GismeteoParser.Parsers;

Console.ForegroundColor = ConsoleColor.Cyan;
Console.WriteLine($"Дата и время запуска приложения: {DateTime.Now}\n");

ParserGismeteo parserGismeteo = new();
parserGismeteo.Parse();

ParserRambler parserRambler = new();
parserRambler.Parse();

//Timekeeper.RunParserWithTimer(60000);
//Timekeeper.RunParserWithThreadingTimer(60000);

Console.ForegroundColor = ConsoleColor.Cyan;
Console.WriteLine($"Дата и время запуска таймера: {DateTime.Now}");
Console.ForegroundColor = ConsoleColor.Yellow;

//костыльный таймер ибо нормальные таймеры в контейнере docker у меня почему-то не работают
CrutchTimer.Run(parserGismeteo, parserRambler, 21600); //таймер срабатывает события каджые 6 часов

//для варианта консольного приложения
//Console.WriteLine("Для остановки таймера введите команду parser stop.\n");

//while (true)
//{
//	string? _ = Console.ReadLine();

//	if (_ == "parser stop")
//	{
//		Timekeeper.aTimer.Stop();
//		Timekeeper.aTimer.Dispose();

//		Console.ForegroundColor = ConsoleColor.Red;
//		Console.WriteLine("Приложение остановлено.");
//		Console.ResetColor();
//		break;
//	}
//	else
//	{
//		Console.WriteLine("Неверная команда. Для остановки таймера введите команду parser stop.\n");
//	}
//};