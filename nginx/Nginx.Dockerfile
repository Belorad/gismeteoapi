FROM nginx:1.27.3-alpine
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
LABEL author="Ivan Pertsev"
LABEL repository="gitlab.com/Belorad"
LABEL project="Weather parser"
LABEL version="0.1"