﻿global using System.ComponentModel.DataAnnotations;
global using System.ComponentModel.DataAnnotations.Schema;

namespace GismeteoWebAPI.Entities
{
	public class ApplicationContext : DbContext
	{
		public DbSet<Cities>? Cities { get; set; }
		public DbSet<Weather>? Weather { get; set; }

		public ApplicationContext(DbContextOptions<ApplicationContext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
		}
	}
}