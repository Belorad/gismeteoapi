﻿using MongoDB.Driver;

namespace GismeteoWebAPI.Services
{
	public class MongoDBService : IMongoDBService
	{
		public MongoClient GetConnection(string connectionString)
		{
			return MongoDB.client = new MongoClient(connectionString);
		}
	}
}
