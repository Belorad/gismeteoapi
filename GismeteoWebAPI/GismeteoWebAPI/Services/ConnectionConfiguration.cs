﻿namespace GismeteoWebAPi.Services
{
	internal class ConnectionConfiguration
	{
		public static string GetConnectionString(string DbName)
		{
			var builder = new ConfigurationBuilder();
			// установка пути к текущему каталогу
			builder.SetBasePath(Directory.GetCurrentDirectory());
			// получаем конфигурацию из файла appsettings.json
			builder.AddJsonFile("appsettings.json");
			// создаем конфигурацию
			var config = builder.Build();
			// получаем строку подключения
			string connectionString = config.GetConnectionString($"{DbName}")!;

			return connectionString;
		}
	}
}
