﻿using MongoDB.Driver;

namespace GismeteoWebAPI.Services
{
	public interface IMongoDBService
	{
		MongoClient GetConnection(string connectionString);
	}
}
