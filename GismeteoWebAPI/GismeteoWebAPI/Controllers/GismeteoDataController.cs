﻿using GismeteoWebAPI.Entities;
using GismeteoWebAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GismeteoWebAPI.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class GismeteoDataController(ApplicationContext context) : ControllerBase
	{
		private readonly ApplicationContext context = context;

		/// <summary>
		/// Взятие погоды Gismeteo на сегодняшнюю дату.
		/// </summary>
		/// <remarks>
		/// Пример ответа:
		/// 
		///		{
		///			"Name": "Москва",
		///			"Date": "2024-12-01",
		///			"Sky": "Ясно",
		///			"MinTemperature": -2,
		///			"MaxTemperature": 0,
		///			"MaxWindVelocity": 5,
		///			"Precipitation: 0
		///		}
		/// </remarks>
		/// <param name="city">Название города</param>
		/// <response code="200">Данные погоды успешно взяты</response>
		/// <response code="400">601 Такого города в базе данных нет
		/// &#xA;602 Данные погоды отсутсвуют</response>
		[HttpGet("GetGismeteoWeatherToday"), AllowAnonymous]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<ReadingGismeteo>> GetGismeteoWeatherToday(string city)
		{
			DateTime day = DateTime.SpecifyKind(DateTime.Today, DateTimeKind.Utc);

			Cities? dataCity;
			try
			{
				dataCity = await context.Cities!.
					AsNoTracking().
					Include(c => c.Weather).
					FirstOrDefaultAsync(c => c.Name == city);
			}
			catch
			{
				Response600s response604 = new()
				{
					Code = 604,
					Result = "Ошибка подключения к БД."
				};

				return BadRequest(response604);
			}


			if (dataCity == null)
			{
				Response600s response601 = new()
				{
					Code = 601,
					Result = "Такого города в базе данных нет."
				};

				return BadRequest(response601);
			}

			var dataWeather = dataCity.Weather!.
				Find(w => w.Date == day);

			if (dataWeather == null)
			{
				Response600s response602 = new()
				{
					Code = 601,
					Result = "Данные погоды отсутствуют."
				};

				return BadRequest(response602);
			}

			ReadingGismeteo result = new()
			{
				Name = dataCity.Name,
				Date = dataWeather.Date,
				Sky = dataWeather.Sky,
				MinTemperature = dataWeather.MinTemperature,
				MaxTemperature = dataWeather.MaxTemperature,
				MaxWindVelocity = dataWeather.MaxWindVelocity,
				Precipitation = dataWeather.Precipitation
			};

			return Ok(result);
		}

		/// <summary>
		/// Взятие погоды Gismeteo на указанную дату.
		/// </summary>
		/// <remarks>
		/// Пример ответа:
		/// 
		///		{
		///			"Name": "Москва",
		///			"Date": "2024-12-01",
		///			"Sky": "Ясно",
		///			"MinTemperature": -2,
		///			"MaxTemperature": 0,
		///			"MaxWindVelocity": 5,
		///			"Precipitation: 0
		///		}
		/// </remarks>
		/// <param name="city">Название города</param>
		/// <param name="date">Выбранная дата</param>
		/// <response code="200">Данные погоды успешно взяты</response>
		/// <response code="400">601 Такого города в базе данных нет
		/// &#xA;602 Данные погоды отсутсвуют</response>
		[HttpGet("GetGismeteoWeatherOnDate"), AllowAnonymous]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<ReadingGismeteo>> GetGismeteoWeatherOnDate(string city, DateTime date)
		{
			DateTime day = DateTime.SpecifyKind(date, DateTimeKind.Utc);

			Cities? dataCity;

			try
			{
				dataCity = await context.Cities!.
					AsNoTracking().
					Include(c => c.Weather).
					FirstOrDefaultAsync(c => c.Name == city);
			}
			catch
			{
				Response600s response604 = new()
				{
					Code = 604,
					Result = "Ошибка подключения к БД."
				};

				return BadRequest(response604);
			}

			if (dataCity == null)
			{
				Response600s response601 = new()
				{
					Code = 601,
					Result = "Такого города в базе данных нет."
				};

				return BadRequest(response601);
			}

			var dataWeather = dataCity.Weather!.
				Find(w => w.Date == day);

			if (dataWeather == null)
			{
				Response600s response602 = new()
				{
					Code = 601,
					Result = "Данные погоды отсутствуют."
				};

				return BadRequest(response602);
			}

			ReadingGismeteo result = new()
			{
				Name = dataCity.Name,
				Date = dataWeather.Date,
				Sky = dataWeather.Sky,
				MinTemperature = dataWeather.MinTemperature,
				MaxTemperature = dataWeather.MaxTemperature,
				MaxWindVelocity = dataWeather.MaxWindVelocity,
				Precipitation = dataWeather.Precipitation
			};

			return Ok(result);
		}

		/// <summary>
		/// Взятие погоды Gismeteo на 10 дней.
		/// </summary>
		/// <remarks>
		/// Пример ответа:
		/// 
		///		{
		///			"Name": "Москва",
		///			"Date": "2024-12-01",
		///			"Sky": "Ясно",
		///			"MinTemperature": -2,
		///			"MaxTemperature": 0,
		///			"MaxWindVelocity": 5,
		///			"Precipitation: 0
		///		},
		///		{
		///			"Name": "Москва",
		///			"Date": "2024-12-02",
		///			"Sky": "Ясно",
		///			"MinTemperature": -4,
		///			"MaxTemperature": -1,
		///			"MaxWindVelocity": 3,
		///			"Precipitation: 0
		///		}
		/// </remarks>
		/// <param name="city">Название города</param>
		/// <response code="200">Данные погоды успешно взяты</response>
		/// <response code="400">601 Такого города в базе данных нет
		/// &#xA;603 Прогноз погоды на 10 дней отсутсвует</response>
		[HttpGet("GetGismeteoWeatherOn10Days"), AllowAnonymous]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<ReadingGismeteo[]>> GetGismeteoWeatherOn10Days(string city)
		{
			Cities? dataCity;
			
			try
			{
				dataCity = await context.Cities!.
					AsNoTracking().
					Include(c => c.Weather).
					FirstOrDefaultAsync(c => c.Name == city);
			}
			catch
			{
				Response600s response604 = new()
				{
					Code = 604,
					Result = "Ошибка подключения к БД."
				};

				return BadRequest(response604);
			}

			if (dataCity == null)
			{
				Response600s response601 = new()
				{
					Code = 601,
					Result = "Такого города в базе данных нет."
				};

				return BadRequest(response601);
			}

			ReadingGismeteo[] result = new ReadingGismeteo[10];

			for (int i = 0; i < 10; i++)
			{
				DateTime day = DateTime.Today.AddDays(i);

				var dataWeather = dataCity.Weather!.
					Find(w => w.Date == DateTime.SpecifyKind(day, DateTimeKind.Utc));

				if (dataWeather != null)
				{
					result[i] = new()
					{
						Name = dataCity.Name,
						Date = dataWeather.Date,
						Sky = dataWeather.Sky,
						MinTemperature = dataWeather.MinTemperature,
						MaxTemperature = dataWeather.MaxTemperature,
						MaxWindVelocity = dataWeather.MaxWindVelocity,
						Precipitation = dataWeather.Precipitation
					};
				}
				else
				{
					Response600s response603 = new()
					{
						Code = 603,
						Result = "Прогноз погоды на 10 дней отсутствует."
					};

					return BadRequest(response603);
				}
			}

			return Ok(result);
		}
	}
}
