﻿using GismeteoWebAPi.Services;
using GismeteoWebAPI.Models;
using GismeteoWebAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace GismeteoWebAPI.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class RamblerDataController(IMongoDBService mongoDBService) : ControllerBase
	{
		private readonly IMongoDBService mongoDBService = mongoDBService;

		/// <summary>
		/// Взятие погоды Rambler на сегодняшнюю дату.
		/// </summary>
		/// <remarks>
		/// Пример ответа:
		/// 
		///		{
		///			"City": "Москва",
		///			"Date": "2024-12-01",
		///			"Temperature": -2,
		///			"WindSpeed": 5,
		///			"WindDirection": "NE",
		///			"Humidity": 96,
		///			"Pressure": 735,
		///			"Icon": "cloudy",
		///			"Precipitation": 0
		///		}
		/// </remarks>
		/// <param name="city">Название города</param>
		/// <response code="200">Данные погоды успешно взяты</response>
		/// <response code="400">601 Такого города в базе данных нет
		/// &#xA;602 Данные погоды отсутсвуют</response>
		[HttpGet("GetRamblerWeatherToday"), AllowAnonymous]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<ReadingRambler>> GetRamblerWeatherToday(string city)
		{
			string today = DateTime.UtcNow.ToString("yyyy-MM-dd");
			IMongoDatabase? db;

			try
			{ 
				db = mongoDBService.GetConnection(ConnectionConfiguration.GetConnectionString("Compose-MongoDB"))
					.GetDatabase("RamblerWeather");
			}
			catch
			{
				Response600s response604 = new()
				{
					Code = 604,
					Result = "Ошибка подключения к БД."
				};

				return BadRequest(response604);
			}

			var collectionCities = db.GetCollection<RamblerMongoDB>("Cities");
			var filterCities = new BsonDocument { { "Name", $"{city}" } };
			using var cursorCities = await collectionCities.FindAsync<CitiesRamblerMongo>(filterCities);

			var finder = await cursorCities.FirstOrDefaultAsync();

			if (finder == null)
			{
				Response600s response601 = new()
				{
					Code = 601,
					Result = "Такого города в базе данных нет."
				};

				return BadRequest(response601);
			}

			var collection = db.GetCollection<RamblerMongoDB>("Weather");
			var filter = new BsonDocument { { "City", $"{city}" }, { "Date", $"{today}" } };
			using var cursor = await collection.FindAsync<RamblerMongoDB>(filter);

			var subresult = await cursor.FirstOrDefaultAsync();

			if (subresult == null)
			{
				Response600s response602 = new()
				{
					Code = 601,
					Result = "Данные погоды отсутствуют."
				};

				return BadRequest(response602);
			}

			ReadingRambler result = new()
			{
				City = subresult.City,
				Date = subresult.Date,
				Temperature = subresult.Temperature,
				WindSpeed = subresult.WindSpeed,
				WindDirection = subresult.WindDirection,
				Humidity = subresult.Humidity,
				Pressure = subresult.Pressure,
				Icon = subresult.Icon,
				Precipitation = subresult.Precipitation
			};

			return Ok(result);
		}

		/// <summary>
		/// Взятие погоды Rambler на указанную дату.
		/// </summary>
		/// <remarks>
		/// Пример ответа:
		/// 
		///		{
		///			"City": "Москва",
		///			"Date": "2024-12-01",
		///			"Temperature": -2,
		///			"WindSpeed": 5,
		///			"WindDirection": "NE",
		///			"Humidity": 96,
		///			"Pressure": 735,
		///			"Icon": "cloudy",
		///			"Precipitation": 0
		///		}
		/// </remarks>
		/// <param name="city">Название города</param>
		/// <param name="date">Выбранная дата</param>
		/// <response code="200">Данные погоды успешно взяты</response>
		/// <response code="400">601 Такого города в базе данных нет
		/// &#xA;602 Данные погоды отсутсвуют</response>
		[HttpGet("GetRamblerWeatherOnDate"), AllowAnonymous]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<ReadingRambler>> GetTheWeatherOnDate(string city, DateTime date)
		{
			string onday = date.ToString("yyyy-MM-dd");
			IMongoDatabase? db;

			try
			{
				db = mongoDBService.GetConnection(ConnectionConfiguration.GetConnectionString("Compose-MongoDB"))
					.GetDatabase("RamblerWeather");
			}
			catch
			{
				Response600s response604 = new()
				{
					Code = 604,
					Result = "Ошибка подключения к БД."
				};

				return BadRequest(response604);
			}

			var collectionCities = db.GetCollection<RamblerMongoDB>("Cities");
			var filterCities = new BsonDocument { { "Name", $"{city}" } };
			using var cursorCities = await collectionCities.FindAsync<CitiesRamblerMongo>(filterCities);

			var finder = await cursorCities.FirstOrDefaultAsync();

			if (finder == null)
			{
				Response600s response601 = new()
				{
					Code = 601,
					Result = "Такого города в базе данных нет."
				};

				return BadRequest(response601);
			}

			var collection = db.GetCollection<RamblerMongoDB>("Weather");
			var filter = new BsonDocument { { "City", $"{city}" }, { "Date", $"{onday}" } };
			using var cursor = await collection.FindAsync<RamblerMongoDB>(filter);

			var subresult = await cursor.FirstOrDefaultAsync();

			if (subresult == null)
			{
				Response600s response602 = new()
				{
					Code = 601,
					Result = "Данные погоды отсутствуют."
				};

				return BadRequest(response602);
			}

			ReadingRambler result = new()
			{
				City = subresult.City,
				Date = subresult.Date,
				Temperature = subresult.Temperature,
				WindSpeed = subresult.WindSpeed,
				WindDirection = subresult.WindDirection,
				Humidity = subresult.Humidity,
				Pressure = subresult.Pressure,
				Icon = subresult.Icon,
				Precipitation = subresult.Precipitation
			};

			return Ok(result);
		}

		/// <summary>
		/// Взятие погоды Rambler на 10 дней.
		/// </summary>
		/// <remarks>
		/// Пример ответа:
		/// 
		///		{
		///			"City": "Москва",
		///			"Date": "2024-12-01",
		///			"Temperature": -2,
		///			"WindSpeed": 5,
		///			"WindDirection": "NE",
		///			"Humidity": 96,
		///			"Pressure": 735,
		///			"Icon": "cloudy",
		///			"Precipitation": 0
		///		},
		///		{
		///			"City": "Москва",
		///			"Date": "2024-12-01",
		///			"Temperature": -4,
		///			"WindSpeed": 3,
		///			"WindDirection": "N",
		///			"Humidity": 96,
		///			"Pressure": 735,
		///			"Icon": "cloudy",
		///			"Precipitation": 0
		///		}
		/// </remarks>
		/// <param name="city">Название города</param>
		/// <response code="200">Данные погоды успешно взяты</response>
		/// <response code="400">601 Такого города в базе данных нет
		/// &#xA;603 Прогноз погоды на 10 дней отсутсвует</response>

		[HttpGet("GetRamblerWeatherOn10Days"), AllowAnonymous]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<ReadingRambler[]>> GetRamblerWeatherOn10Days(string city)
		{
			IMongoDatabase? db;

			try
			{
				db = mongoDBService.GetConnection(ConnectionConfiguration.GetConnectionString("Compose-MongoDB"))
					.GetDatabase("RamblerWeather");
			}
			catch
			{
				Response600s response604 = new()
				{
					Code = 604,
					Result = "Ошибка подключения к БД."
				};

				return BadRequest(response604);
			}

			var collectionCities = db.GetCollection<RamblerMongoDB>("Cities");
			var filterCities = new BsonDocument { { "Name", $"{city}" } };
			using var cursorCities = await collectionCities.FindAsync<CitiesRamblerMongo>(filterCities);

			var finder = await cursorCities.FirstOrDefaultAsync();

			if (finder == null)
			{
				Response600s response601 = new()
				{
					Code = 601,
					Result = "Такого города в базе данных нет."
				};

				return BadRequest(response601);
			}

			var collection = db.GetCollection<RamblerMongoDB>("Weather");

			RamblerMongoDB subresult = new();
			ReadingRambler[] result = new ReadingRambler[10];

			for (int i = 0; i < 10; i++)
			{
				string day = DateTime.UtcNow
					.AddDays(i)
					.ToString("yyyy-MM-dd");

				var filter = new BsonDocument { { "City", $"{city}" }, { "Date", $"{day}" } };
				using var cursor = await collection.FindAsync<RamblerMongoDB>(filter);

				subresult = await cursor.FirstOrDefaultAsync();

				if (subresult != null)
				{
					result[i] = new ReadingRambler()
					{
						City = subresult.City,
						Date = subresult.Date,
						Temperature = subresult.Temperature,
						WindSpeed = subresult.WindSpeed,
						WindDirection = subresult.WindDirection,
						Humidity = subresult.Humidity,
						Pressure = subresult.Pressure,
						Icon = subresult.Icon,
						Precipitation = subresult.Precipitation
					};
				}
				else
				{
					Response600s response603 = new()
					{
						Code = 603,
						Result = "Прогноз погоды на 10 дней отсутствует."
					};

					return BadRequest(response603);
				}
			}

			return Ok(result);
		}
	}
}
