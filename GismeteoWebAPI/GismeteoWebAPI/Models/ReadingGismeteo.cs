﻿namespace GismeteoWebAPI.Models
{
	public class ReadingGismeteo
	{
		public string? Name { get; set; }
		public DateTime? Date { get; set; }
		public string? Sky { get; set; }
		public string? MinTemperature { get; set; }
		public string? MaxTemperature { get; set; }
		public string? MaxWindVelocity { get; set; }
		public string? Precipitation { get; set; }
	}
}
