﻿using MongoDB.Bson;

namespace GismeteoWebAPI.Models
{
	public class RamblerMongoDB
	{
		public ObjectId Id { get; set; }
		public string City { get; set; } = string.Empty;
		public string Date { get; set; } = string.Empty;
		public int Temperature { get; set; }
		public int WindSpeed { get; set; }
		public string WindDirection { get; set; } = string.Empty;
		public int Humidity { get; set; }
		public int Pressure { get; set; }
		public string Icon { get; set; } = string.Empty;
		public double Precipitation { get; set; }
	}
}
